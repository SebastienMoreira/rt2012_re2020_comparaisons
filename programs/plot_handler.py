#=========================================
# coding = utf-8
#=========================================
# Name         : plot_handler.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 03/10/2019
# Update       : 07/10/2019
# Description  : Class used for plotting data more easily
#========================================

#========================================
# Links
#========================================

#

#========================================
# Packages
#========================================

# Python core packages/libraries/modules
try:
    import os
except ImportError:
    raise ImportError("Sorry, but the \"os\" module/package is required to run this program!")
try:
    import operator
except ImportError:
    raise ImportError("Sorry, but the \"operator\" module/package is required to run this program!")
try:
    import pandas as pd 
except ImportError:
    raise ImportError("Sorry, but the \"pandas\" module/package is required to run this program!")
try:
    import numpy as np
except ImportError:
    raise ImportError("Sorry, but the \"numpy\" module/package is required to run this program!")
try:
    import matplotlib
except ImportError:
    raise ImportError("Sorry, but the \"matplotlib\" module/package is required to run this program!") 
try:
    import matplotlib.pyplot as plt
except ImportError:
    raise ImportError("Sorry, but the \"matplotlib\" module/package is required to run this program!") 
try:
    import seaborn
except ImportError:
    raise ImportError("Sorry, but the \"seaborn\" module/package is required to run this program!")   
try:
    import copy
except ImportError:
    raise ImportError("Sorry, but the \"copy\" module/package is required to run this program!")
try:
    import collections
except ImportError:
    raise ImportError("Sorry, but the \"collections\" module/package is required to run this program!")
    
    
# Python extension packages/libraries/modules

# Set the figure to the default style 
matplotlib.style.use("classic")

#========================================
# Classes
#========================================

# Initialize the global variable "NUMBER_OF_GRAPHS" to avoid overlapping
NUMBER_OF_GRAPHS = 0

# Class used for plotting data more easily
class PlotHandler:
    """
    Class used for plotting data more easily
    ...
    Constants
    ---------
    None
    
    Class attributes
    ----------------
    None
    
    Instance attributes
    -------------------
    None
    
    Methods
    -------
    display_violin_plot(self, indicators, vertical_axis_name, horizontal_axis_name):
        Display a violin plot figure (and save it at the predefined location)
    """
    
    # Initialize (construct) a PlotHandler object
    def __init__(self, dataframe, usages, systems, figures_path):
        """
        Initialize (construct) a PlotHandler object
        -------------------------------------------------------------
        Parameters
        ----------
        dataframe: pandas.DataFrame
            Dataframe containing the data
        usages: list
            List of conditions on the usages
        systems: list 
            List of conditions on the systems
        figures_path: str
            Path to the directory where the figure will be saved
            
        Returns
        -------
        PlotHandler object

        Example
        -------
        >>> PlotHandler(dataframe, ["mi", "lc"], [], ".//data//")
        """
        # Copy dataframe  (in case we could need the initial one later)
        self.dataframe_temporary    = copy.deepcopy(dataframe)
        # Store initial dataframe in memory (just in case)
        self.dataframe              = dataframe
        # Save other parameters
        self.usages                 = usages
        self.systems                = systems
        self.figures_path           = figures_path

    # Display a violin plot figure (and save it at the predefined location)
    def display_violin_plot(self, indicators, vertical_axis_name, horizontal_axis_name, axes = None):
        """
        Display a violin plot figure (and save it at the predefined location)
        -------------------------------------------------------------
        Parameters
        ----------
        indicators: list
            List of indicators for the horizontal and vertical axes
        vertical_axis_name: str
            Name of the vertical axis
        horizontal_axis_name: str 
            Name of the horizontal axis
            
        Returns
        -------
        None

        Example
        -------
        >>> display_violin_plot(["system", "bbio"], "Typologies [-]", "Bbio [-]")
        """  
        # Filter dataframe based on usages and systems
        self._filtered_dataframe(self.usages)
        self._filtered_dataframe(self.systems)
        # Generate figure
        self._generate_figure(indicators, vertical_axis_name, horizontal_axis_name, axes)
        
    # Filter a dataframe based on some conditions (list of conditions)
    def _filtered_dataframe(self, conditions):
        """
        Filter a dataframe based on some conditions (list of conditions)
        -------------------------------------------------------------
        Parameters
        ----------
        conditions: list
            List of tuples, each containing a condition in the following format : (condition, operator, value)

        Returns
        -------
        None

        Example
        -------
        >>> _filtered_dataframe([("usage", "==", "mi")])
        """       
        if conditions: 
            # Filter the dataframe by usage (keep only the desired ones)
            mask = np.logical_or.reduce([self._get_operator(condition[1])(self.dataframe_temporary[condition[0]], condition[2]) for condition in conditions])
            # Keep only rows which satisfy the conditions
            self.dataframe_temporary = self.dataframe_temporary[mask]

    # Return a python operator based on some string operator
    def _get_operator(self, operator_string):
        """
        Return a python operator based on some string operator
        -------------------------------------------------------------
        Parameters
        ----------
        operator_string: str
            Operator as a string (e.g., "==")

        Returns
        -------
        An operator object (e.g., operator.eq)

        Example
        -------
        >>> _get_operator("==")
        """       
        # Dictionary of operators
        dictionary_of_operators = {"<=" : operator.lt,
                                   "==" : operator.eq}
        # Return the desired operator
        return dictionary_of_operators[operator_string]
    
    # Generate a violing plot 
    def _generate_figure(self, indicators, vertical_axis_name, horizontal_axis_name, axes):
        """
        Generate a violing plot 
        -------------------------------------------------------------
        Parameters
        ----------
        indicators: list
            List of indicators for the horizontal and vertical axes
        vertical_axis_name: str
            Name of the vertical axis
        horizontal_axis_name: str 
            Name of the horizontal axis

        Returns
        -------
        None

        Example
        -------
        >>> _generate_figure(["system", "bbio"], "Systèmes [-]", "Bbio [-]")
        """
        # Use the global variable "NUMBER_OF_GRAPHS"
        global NUMBER_OF_GRAPHS
        # Figure
        figure      = plt.figure(num = NUMBER_OF_GRAPHS, figsize = (11.5, 7.5), facecolor = 'w', edgecolor = 'k')
        # Set up axis
        axis        = figure.add_subplot(111) # number of rows, number of columns, and plot number
        # Create empty dictionary
        dataframe = dict()
        # For each indicator, get data
        for key in collections.Counter(self.dataframe_temporary[indicators[0]]):
            dataframe[key] = copy.deepcopy(self.dataframe_temporary[self.dataframe_temporary[indicators[0]] == key][indicators[1]])        
        # Generate violin plots
        seaborn.violinplot(data = pd.DataFrame(dataframe))
        #
        if axes != None:
            axis.set_ylim(axes[0][0], axes[0][1])
            axis.set_yticks(np.arange(axes[1][0], axes[1][1], axes[1][2]))
        axis.set_xlabel(vertical_axis_name, fontsize = 11) 
        axis.set_ylabel(horizontal_axis_name, fontsize = 11)
        plt.savefig(self.figures_path + os.sep + indicators[1].lower() + "_" + indicators[0].lower() + ".png", format = "png", dpi = 300)
        # Increment the number of graphs
        NUMBER_OF_GRAPHS += 1

#========================================
# Functions
#========================================
     

 

        
  





    
    

    









    
    