#=========================================
# coding = utf-8
#=========================================
# Name         : analyzer.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 03/10/2019
# Update       : 07/10/2019
# Description  : Class to analyze re-simulated xml cases
#========================================

#========================================
# Links
#========================================

#

#========================================
# Packages
#========================================

# Python core packages/libraries/modules
try:
    import os
except ImportError:
    raise ImportError("Sorry, but the \"os\" module/package is required to run this program!")
try:
    import pandas as pd 
except ImportError:
    raise ImportError("Sorry, but the \"pandas\" module/package is required to run this program!")

# Python extension packages/libraries/modules
try:
    import pycometh
except ImportError:
    raise ImportError("Sorry, but the \"pycometh\" module/package is required to run this program!") 
try:
    from buildings_handler import BuildingsHandler
except ImportError:
    raise ImportError("Sorry, but the \"building\" module/package is required to run this program!") 

#========================================
# Classes
#========================================

# Class to analyze re-simulated xml cases
class Analyzer:
    """
    Class to analyze re-simulated xml cases
    ...
    Constants
    ---------
    None
    
    Class attributes
    ----------------
    None
    
    Instance attributes
    -------------------
    None
    
    Methods
    -------
    get_results(self):
        Get results based on the given output directory and usages
    """
    
    # Initialize (construct) a Analyzer object
    def __init__(self, output_directory_path, desired_building_characteristics, usages):
        """
        Initialize (construct) a Analyzer object
        -------------------------------------------------------------
        Parameters
        ----------
        output_directory_path: str
            Path to the directory where all the simulations have been performed
        desired_building_characteristics: list
            List of building characteristics (outputs of interest from the xml file) under the following format (name, path, unit)
        usages: list
            List of usages of interest under the following format (name, path, values) - the name must be equal to the name of the directory where the simulations are stored
            
        Returns
        -------
        Analyzer object

        Example
        -------
        >>> Analyzer("C://Users//outputs", [("cep","/Sortie_Batiment_C/O_Cep_annuel","kwh/(m².an)")], [("mi", "/Sortie_Zone_C/Usage", [1,2]), ("lc", "/Sortie_Zone_C/Usage", [3])])
        """
        # Save parameters 
        self._output_directory_path             = output_directory_path
        self._desired_building_characteristics  = desired_building_characteristics
        self._usages                            = usages 

    # Get results based on the given output directory and usages
    def get_results(self):
        """
        Get results based on the given output directory and usages
        -------------------------------------------------------------
        Parameters
        ----------
        None
        
        Returns
        -------
        self._dataframe: pandas.DataFrame
            Dataframe containing all the results (row = simulation / column = output/buildinc characteristic of interest)

        Example
        -------
        >>> get_results()
        """
        # Create an empty directory to store the results per usage
        self._dataframe = dict()
        # Read results
        self._read_results()
        # Return the dataframe containing the results
        return self._dataframe
        
    # Read results based on the given output directory and usages
    def _read_results(self):
        """
        Read results based on the given output directory and usages
        -------------------------------------------------------------
        Parameters
        ----------
        None
        
        Returns
        -------
        None

        Example
        -------
        >>> _read_results()
        """
        # Create an empty directory to store the results
        self._results = dict()
        # Iterate through each usage
        for usage in self._usages:
            self._read_results_per_usage(usage)
            # Store the results (dictionary) in a dataframe
            self._dataframe[usage[0]] = pd.DataFrame(self._results[usage[0]])
            # Transform the dataframe to have each case as a row
            self._dataframe[usage[0]] = self._dataframe[usage[0]].T
            # Set the index name
            self._dataframe[usage[0]].index.name = "index"

            # Read results based on the given usage
    def _read_results_per_usage(self, usage):
        """
        Read results based on the given usage
        -------------------------------------------------------------
        Parameters
        ----------
        usage: tuple
            Tuple of usages of interest under the following format (name, path, values) - the name must be equal to the name of the directory where the simulations are stored
        
        Returns
        -------
        None

        Example
        -------
        >>> _read_results_per_usage(("mi", "/Sortie_Zone_C/Usage", [1,2]))
        """
        # Create a building object to store/handle the results 
        self._buildings_handler     = BuildingsHandler()
        # Scan directory where simulations have been run 
        dataframe_scanner           = pycometh.exp_plan.scan_exp_plan(self._output_directory_path + os.sep + usage[0])
        # For each simulation, create a RT2012Sortie object to get the outputs of interest
        for index, row in dataframe_scanner.iterrows():
            # Check if the simulation has run properly
            if row.is_success == True:
                with open(r"C:\Users\rey\Desktop\debug.txt", 'w') as fd:
                    fd.write(usage[0] + ": " + str(row.sub_dir_0))
                # Create a RT2012Sortie object
                rt_outputs = pycometh.Rt2012Sortie(row.output_file)
                # Get the outputs of interest (values from xml file associated with a given tag) for this specific file
                self._get_xml_outputs(rt_outputs, row, usage)
                # Update the counter to properly store the results
                self._buildings_handler.update_counter()
                #with open(r"..\cases\rt2012\already_run.json", 'a') as fd:
                #    fd.write("\"" + str(row.sub_dir_0) + "\",\n")
            else:
                pass
                #with open(r"..\cases\rt2012\error.json", 'a') as fd:
                #     fd.write("\"" + str(row.sub_dir_0) + "\",\n")
        # Store the results per usage
        self._results[usage[0]] = self._buildings_handler.get_buildings_characteristics()
    
    # Get the xml outputs of interest (values from xml file associated with a given tag) 
    def _get_xml_outputs(self, rt_outputs, row, usage):
        """
        Get the xml outputs of interest (values from xml file associated with a given tag) 
        -------------------------------------------------------------
        Parameters
        ----------
        rt_outputs: Rt2012Sortie object
            Object containing functions to deal with a particular RT2012 file (which has been run)
        usage: tuple
            Tuple of usages of interest under the following format (name, path, values) - the name must be equal to the name of the directory where the simulations are stored
        
        Returns
        -------
        None

        Example
        -------
        >>> _get_xml_outputs(rt_outputs, ("mi", "/Sortie_Zone_C/Usage", [1,2]))
        """
        # For each output of interest, get its value in the xml file
        for desired_output in self._desired_building_characteristics:
            usages = rt_outputs.get_xml_values(usage[1])






            #if len(list(set(usages))) > 1:
            #if usage[2]


            # Get a table of xml object collections
            #table_of_xml_object_collections = rt_outputs.build_collections_tables()



            # Check the usage for each building (keep only buildings with the proper usage)
            #indices = []
            #for index, row in table_of_xml_object_collections["Sortie_Batiment_C"].iterrows():
            #    if [usage for usage in table_of_xml_object_collections["Sortie_Zone_C"][table_of_xml_object_collections["Sortie_Zone_C"].sortie_batiment_c_index == index]["Usage"]][0] in usage[2]:
            #        indices.append(index)

            #print(indices)
            if all(element in usage[2] for element in usages):
                # Set the directory associated with the simulation under study
                self._buildings_handler.set_directory_name(str(row.sub_dir_0))
                # Get the outputs of interest
                outputs = rt_outputs.get_xml_values(desired_output[1])
                # Reset the counter (counting the total number of buildings)
                self._buildings_handler.reset_counter()
                # Get the outputs from xml file under a specific shape
                self._shape_xml_outputs(outputs, desired_output[0])

            # self._shape_xml_outputs(outputs[indices], desired_output[0])
            
    # Get the outputs from xml file under a specific shape
    def _shape_xml_outputs(self, outputs, desired_output):
        """
        Get the outputs from xml file under a specific shape
        -------------------------------------------------------------
        Parameters
        ----------
        outputs: list
            List of values (outputs of interest from the xml file)
        desired_output: str 
            Name of the desired output 
        
        Returns
        -------
        None

        Example
        -------
        >>> _shape_xml_outputs([44,53], "cep")
        """
        # For each output, store it properly using buildings_handler
        for output in outputs:
            self._buildings_handler.set_building_characteristic(desired_output, output)        
    