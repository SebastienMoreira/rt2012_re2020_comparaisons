#=========================================
# coding = utf-8
#=========================================
# Name         : analyze_rt2012_re2020.py
# Author       : Anthony Rey
# Version      : 1.0.0.0
# Date         : 09/10/2019
# Update       : 09/10/2019
# Description  : Analyze the RT2012 and RE2020 simulations (re-simulated xml cases)
#========================================

#========================================
# Links
#========================================

#

#========================================
# Packages
#========================================

# Python core packages/libraries/modules
try:
    import os
except ImportError:
    raise ImportError("Sorry, but the \"os\" module/package is required to run this program!")
try:
    import pandas as pd 
except ImportError:
    raise ImportError("Sorry, but the \"pandas\" module/package is required to run this program!")

# Python extension packages/libraries/modules
try:
    import pycometh
except ImportError:
    raise ImportError("Sorry, but the \"pycometh\" module/package is required to run this program!") 
try:
    from analyzer import Analyzer
except ImportError:
    raise ImportError("Sorry, but the \"analyzer\" module/package is required to run this program!")
try:
    from plot_handler import PlotHandler
except ImportError:
    raise ImportError("Sorry, but the \"plot_handler\" module/package is required to run this program!")


#========================================
# Classes
#========================================

#========================================
# Functions
#========================================

def main():
    print("\nSTART\n")

    # Parameters
    directory_path                          = "..//cases"
    case_study                              = "rt2012"
    building_characteristics                = [("cep", "/Sortie_Batiment_C/O_Cep_annuel", "kWh/m²"), ("bbio", "/Sortie_Batiment_B/O_Bbio_pts_annuel", "-")]
    usages                                  = [("mi", "/Sortie_Zone_C/Usage", [1]), ("lc", "/Sortie_Zone_C/Usage", [2]), ("bu", "/Sortie_Zone_C/Usage", [16]), ("ens", "/Sortie_Zone_C/Usage", [4,5,6,7])]
    # Build analyzer
    analyzer_rt2012_re2020                  = Analyzer(os.path.join(directory_path, case_study, "outputs"), building_characteristics, usages)
    # Store results in a dictionary (keys = usages)
    dictionary_of_results                   = analyzer_rt2012_re2020.get_results()
    # Rename dictionary's keys
    keys = {"mi":"Maisons individuelles", "lc":"Logements collectifs", "bu":"Bureaux", "ens":"Etablissement d'enseignement"}
    # Set the usage for each case
    list_of_dictionaries = []
    for usage in usages:
        dictionary_of_results[usage[0]]["usage"] = keys[usage[0]]
        list_of_dictionaries.append(dictionary_of_results[usage[0]])
        dictionary_of_results[usage[0]].to_csv(os.path.join("..//data", case_study, "dataframes_outputs", "dataframe_" + usage[0] + ".csv"))
        print(len(dictionary_of_results[usage[0]]["usage"]))
    # Store everything in a dataframe
    dataframe = pd.concat(list_of_dictionaries)
    # Build a plot handler to generate figures
    analysis                                = PlotHandler(dataframe, [], [], os.path.join("..//figures", case_study))
    analysis.display_violin_plot(["usage", "cep"], "Usages [-]", "Cep [kWh/m²]", [(0,350),(0,400,50)])
    analysis.display_violin_plot(["usage", "bbio"], "Usages [-]", "Bbio [-]")
    # Test commit
    print("\nEND\n")


if __name__ == "__main__":
    main()